package com.andromet.radiobutton;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    RadioButton radio1, radio2, radio3;
    Switch switch1, switch2;
    RadioGroup radioGroup1;

    int counter = 0;
    boolean switchOn = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void onSwitch1Clicked(View view) {

        radio1 = findViewById(R.id.radio1);
        radio2 = findViewById(R.id.radio2);
        radio3 = findViewById(R.id.radio3);

        switch1 = findViewById(R.id.switch1);
        switch2 = findViewById(R.id.switch2);

        radioGroup1 = findViewById(R.id.radioGroup1);

        if (switch1.isChecked() && counter == 0){
            radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {

                    radio1.setChecked(true);
                    radio2.setChecked(true);
                    radio3.setChecked(true);
                }
            });
            switch2.setChecked(false);
            switch2.setEnabled(false);
            radio1.setChecked(true);
            radio2.setChecked(true);
            radio3.setChecked(true);
            Toast.makeText(getApplicationContext(), "All checked", Toast.LENGTH_SHORT).show();
        }

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){

            @Override
            public void onCheckedChanged(CompoundButton view, boolean i) {

                if(switch1.isChecked())
                {
                    switchOn = true;
                    radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup radioGroup, int i) {

                            radio1.setChecked(true);
                            radio2.setChecked(true);
                            radio3.setChecked(true);
                        }
                    });
                    switch2.setChecked(false);
                    switch2.setEnabled(false);
                    radio1.setChecked(true);
                    radio2.setChecked(true);
                    radio3.setChecked(true);
                    Toast.makeText(getApplicationContext(), "All checked", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    switchOn = false;
                    radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup radioGroup, int i) {

                            radio1.setClickable(true);
                            radio2.setClickable(true);
                            radio3.setClickable(true);
                        }
                    });
                    switch2.setEnabled(true);
                    radio1.setChecked(false);
                    radio2.setChecked(false);
                    radio3.setChecked(false);
                    Toast.makeText(getApplicationContext(), "All unchecked", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    public void onRadioButtonClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();

        if(!switchOn)
        {
            switch (view.getId())
            {
                case R.id.radio1:
                    if (checked)
                    {
                        Toast.makeText(getApplicationContext(), "radio1", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case R.id.radio2:
                    if(checked)
                    {
                        Toast.makeText(getApplicationContext(), "radio2", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case R.id.radio3:
                    if(checked)
                    {
                        Toast.makeText(getApplicationContext(), "radio3", Toast.LENGTH_SHORT).show();
                    }
                    break;
                    default:
                        break;
            }
        }
    }

}
